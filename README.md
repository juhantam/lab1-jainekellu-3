# Team JæineKellu:
1. Henri Sellis
2. Juhan Tamm
3. Joosep Postov

## Homework 1:
<When you finish your homework, add a link pointing to the solution here (for example, a link to a page on the wiki)> 

## Homework 2:
<Links to the solution>

## Homework 3:
<Links to the solution>

## Homework 4:
<Links to the solution>

## Homework 5:
<Links to the solution>

## Homework 6:
<Links to the solution>

## Homework 7:
<Links to the solution>

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)